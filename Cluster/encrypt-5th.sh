#!/bin/bash
echo Upgrade jupyterhub for enki cluster using Helm and config-*.yaml
echo Stable release is 0.9.0
echo Upgrade to add nbgitpuller, https encryption and gitlab authentication ...
helm upgrade jhub jupyterhub/jupyterhub \
  --version=0.9.0 \
  --values config-encrypt.yaml
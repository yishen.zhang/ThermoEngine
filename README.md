# ThermoEngine README
The ThermoEngine repository contains Python packages, C code and header files and C++ and Objective-C class implementations that implement thermodynamic properties of minerals, fluids, and melts.  The repository also includes generic phase equilibrium calculators as well as phase equilibrium calculators that implement the MELTS, pMELTS, MELTS+DEW, and Stixrude-Lithgow-Bertelloni thermodynamic model/data collections. Examples in Jupyter notebooks demonstrate how to call the class methods using Python wrappers.

This README explains how to access a pre-built version of the package, compile the C and objective-C code and build the Python wrappers. For API documentation, see the [Thermoengine documentation](https://enki-portal.gitlab.io/ThermoEngine/).      

Please see the README file in the Cluster folder for details on how to build and deploy the ThermoEngine package on Google cloud and how to create a MiniKube kubernetes cluster on your local computer.

Please see the README files in the Notebooks folder for descriptions of Jupyter notebooks that illustrate various uses and capabilities of the ThermoEngine package.

If you are interested in contributing to this software project, please consult the CONTRIBUTING document.

Repository configuration and maintenance, and the relationship of this repository to others in the ENKI-portal group, is described in the MAINTENANCE document.

This software is released under the GNU Affero General Public License ([GNU AGPLv3](https://www.gnu.org/licenses/agpl-3.0.html)).

## Contents  
[Accessing a pre-built implementation of this software package](#Accessing-a-pre-built-implementation-of-this-software-package)
- [Cloud Server](#Cloud-Server)  
- [Running a container image locally](#Running-a-container-image-locally)  

[Building and installing the code and testers](#building-and-installing-the-code-and-testers)
- [Prerequisites](#prerequisites)  
- [Compilation](#compilation)  
- [Installation](#installation)   
- [Xcode builds](#xcode-builds)   

[Docker image](#docker-image)  
[Building the Python package](#building-the-python-package)  
[Examples (Jupyter notebooks)](#examples-jupyter-notebooks)  

## Accessing a pre-built implementation of this software package

### Cloud Server
The Thermoengine package is accessible from a Google Cloud JupyterHub server. Point your browser to [ENKI-portal](https://server.enki-portal.org/hub/login) and login with your GitLab credentials. If you lack credentials, [register for a free GitLab account](#https://gitlab.com/users/sign_up) to obtain your username and password. 

### Running a container image locally
The ThermoEngine package is available as a pre-built [Docker container image](#docker-image). You can run this image locally to use ThermoEngine software, execute Jupyter notebooks, and to develop new applications.  All this can be done with minimal system configuration. 
- Download and install [Docker Desktop](#https://www.docker.com/get-started).  This step need only be done once and your system may already have Docker installed.
- Clone the ThermoEngine repository to a convenient location on your computer. You will need a [Git](#https://git-scm.com) installation to do this, and most likely it is already avaialble on your system (Macs and Linux machines come preinstalled with Git). To clone the repository, open a terminal window, navigate to a location to store the package, and execute the command
```
git clone https://gitlab.com/ENKI-portal/ThermoEngine.git
```
- After cloning the package, change directory down into the root of the repository (e.g., on Linux and Mac)
```
cd ThermoEngine
```
- At this file location run the shell script
```
./run_docker_locally.sh
```
The first time you execute this script, the complete Docker image will be fetched from the server. This might take some time. Subsequent invocations of the script will only fetch updates and should complete much faster. The script will provide a URL that should be pasted into a browser. This URL will give access to the local Docker container running JupyterLab with an installed ThermoEngine package and its Python/dynamic library dependencies. The container will have read/write access to the files in the ThermoEngine root and directory tree.  

The shell script referenced above accepts additional arguments.  Pass "help" as an argument to see options.  

Remember to update the cloned version of this repository on a regular basis. The command
```
git pull
```
executed from the root repository directory, will download changed files and keep your local version in sync with the container image. 

## Building and installing the code and testers
These instructions pertain to a Macintosh system with Xcode installed. For an Ubuntu-based Linux system, follow the workflow in the Docker file included in the Cluster folder of this project.

### Prerequisites
The build requires that the `gsl` library is installed.  This library can be obtained from Homebrew (Macintosh) or downloaded directly from [GNU](https://www.gnu.org/software/gsl/). The build assumes that the `gsl` library is installed into **/usr/local**.

* NOTE: the gsl library downloaded using pip or conda do not function (perhaps because they place the library in the wrong location). Instead using Homebrew to place the gsl library in the correct location.

### Compilation
You can compile the Objective-C library in the root project directory by executing the command:
```
make
```
### Installation
You can install the library into **/usr/local/lib** (where Python can locate it) with the command:
```
make install
```
You can install the **thermoengine** python interface package with the command:
```
make pyinstall
```
### Testing and code coverage
```
make tests
```
### Xcode builds
You can also build the package on Macintosh systems using Xcode.  Launch Xcode and open the ThermoEngine.workspace project.  Schema are provided to build dynamic libraries and test routines.  
## Docker Image
A fully configured and working executable of the project software is available as a Docker image. The image is named
```
registry.gitlab.com/enki-portal/thermoengine:latest
```
Please consult the README file in the Cluster directory for instructions on deploying this image.
## Building the Python package
Accessing the Objective-C library from Python requires installing the Rubicon-ObjC package. You can easily install this package if you have the Anaconda (conda) software package manager.  

If Anaconda is installed and if the root environment is based upon Python 2.7, you must create a new environment based on Python 3.7+ in order to install the ThermoEngine package. For example, execute these commands from a terminal window to create a new environment (python37) and to activate that Python 3.7 environment:
```
conda create -n python37 python=3.7 anaconda
source activate python37
```
Any Python 3.7+ environment works with Rubicon. Simply substitute the correct version numbers in the above commands.
  If you want to switch back to the original Python 2.7 environment, execute this command:
```
source activate root
```
Execute the following to install the latest version of Rubicon-ObjC 2.10 from [pypi](https://pypi.python.org/pypi):
```
pip install rubicon-objc==0.2.10
```
 Do not install a later version of rubicon-objc, as these are incompatible with the ThermoEngine Python package. Once installed, the package is available to Jupyter notebooks running the Python 3.7+ kernel on your local machine.  

 If you have a traditional Python 3.7+ installation, the above `pip` command will also install Rubicon, but additional Python dependencies may require manual installation.

## Examples (Jupyter notebooks)  
In the Notebooks folder, there are numerous Jupyter notebooks that demonstrate how to call the class methods using Python wrappers. For more details, see the [ThermoEngine documentation](https://enki-portal.gitlab.io/ThermoEngine/examples.html).


cpx_comp = phase_comps['Cpx']
print(2*(cpx_comp[0]-.5*cpx_comp[2]))
print(2*(cpx_comp[-1]-.5*cpx_comp[-2]+cpx_comp[1]))
print((10*cpx_comp))

Nend = cpx_comp.shape[0]
wt = 2*np.random.rand(Nend)-1
np.dot(cpx_comp.T, wt)
cpx_comp-cpx_comp[0]
cpx_comp
np.linalg.svd(cpx_comp-cpx_comp[1])

np.linalg.qr(cpx_comp-cpx_comp[0])

iphs = phases['Cpx']
iphs.endmember_num
iphs.props['species_num']
iphs.props['species_name']
# elem_comp = iphs.props['element_comp']

sys_elems
phase_comps

phases



# +
# bulk_comp = np.array([0.59760393, 0.01614512, 0.04809849, 0.09232406, 0.1571301 ,
#        0.03304928, 0.05565109])

# +
# phase_comps
# -


# # Get minimum energy assemblage

# +
# mu[:,np.newaxis]
# -




# +


def get_init_assem(bulk_comp, comp_endmem, mu_endmem,
                   reduced_assem=False, chempot_init=None, Ashft=1e3, endmem_ids=endmem_ids):


    if chempot_init is None:
        output = np.linalg.lstsq(comp_endmem, mu_endmem, rcond=None)
        chempot_init = output[0]

    chempot = chempot_init.copy()


    # mu_endmem_mod = np.dot(comp_endmem,chempot)
    # inds = np.argsort(mu_endmem-mu_endmem_mod)
    # ind_good = inds[:15]
    # output = np.linalg.lstsq(comp_endmem[ind_good,:], mu_endmem[ind_good], rcond=None)
    # chempot = output[0]


    while True:
        comp_soln, X_soln, Aff_soln, Aff_endmem, phase_names_soln, extras = update_phase_affinities(chempot)
        comp_curr, dmu_curr =  get_current_phase_mu(Aff_soln, Aff_endmem, comp_soln, Ashft=Ashft)



        phs_endmem_sym =  get_phase_endmem_sym(phs_sym, endmem_ids)
        phase_name_curr = np.hstack((phase_names_soln, phs_endmem_sym))
        phase_name_parent = np.hstack((phase_names_soln, phs_sym))


        # BIG TEST OF FIXED ENDMEM mu values
        # NOT good to replace local mu with actual endmem
        # print(dmu_curr)
        # dmu_curr[Nphs:] = mu_endmem-np.dot(comp_endmem, chempot) + Ashft
        # print(dmu_curr)

        dchempot_assem, Gtot, wt_assem, rnorm_assem = find_min_assemblage(
            bulk_comp, comp_curr, dmu_curr, chempot=0)


        # imu = iphs.gibbs_energy(T,P,mol=iX, deriv={'dmol':1})

        # print('Gtot = ', Gtot)
        chempot += dchempot_assem


        # plt.figure()
        # plt.plot(wt_assem, 'ko-')




        inds = np.argsort(Aff_soln)
        # print(phase_names_soln[inds][:rank])
        # print(Aff_soln[inds][:rank])
        # print(phase_names_soln[inds][rank:])
        # print(Aff_soln[inds][rank:])

        Aff_s = Aff_soln[inds]
        phase_stable = phase_names_soln[inds][Aff_s<Ashft]
        Nstable = np.sum(Aff_soln<Ashft)


        phase_name_assem = phase_name_parent[wt_assem>0]
        valid_stable = Nstable <= rank
        valid_assem = np.all([iphsnm in phase_stable for iphsnm in phase_name_assem])

        if valid_stable and valid_assem:
            # print(rnorm_assem)

            # from IPython.core.debugger import set_trace; from IPython import embed; embed(); set_trace()

            # print('valid_assem = ', valid_assem)
            # print('valid_stable = ', valid_stable)
            #
            # print('phase_stable = ', phase_stable)
            # print(Aff_s[Aff_s<Ashft])


            # print('phs wts: ',phase_name_curr[wt_assem>0])
            # print('phs assem: ',phase_name_assem)
            # print('phs stable: ',phase_stable)

            # print('dchempot = ', dchempot_assem)
            break



    # if reduced_assem:
    #     phase_name_assem = []
    #     ind_assem =0
    #
    #     while True:
    #         phase_name_assem



    wt_stable = wt_assem[wt_assem>0]
    comp_assem = comp_curr[wt_assem>0]
    phase_name_assem = phase_name_parent[wt_assem>0]
    # print(phase_name_assem)
    # print(phase_name_parent)

    # REorder phases so that Pure phases are at the end as required
    endmem_num = []
    phase_stable0 = []
    for iphsnm in phase_name_assem:
        if iphsnm not in phase_stable0:
            iphs = phases[iphsnm]
            endmem_num.append(iphs.endmember_num)
            phase_stable0.append(iphsnm)

    endmem_num = np.array(endmem_num)
    phase_stable0 = np.array(phase_stable0)
    # print(endmem_num)

    phase_stable = np.hstack((phase_stable0[endmem_num>1], phase_stable0[endmem_num==1]))
    # print(phase_stable)

    # phase_stable = np.unique(phase_name_assem)



    Nelems = len(chempot)
    Nstable = len(phase_stable)

    Nphases = len(extras['X'].keys())
    wt_endmems = wt_assem[Nphases:]


    nmol_elems = np.zeros((Nstable,Nelems))

    nmol_stable = []
    comp_endmem_stable = []

    endmem_num = []

    for ind, iphsnm in enumerate(phase_stable):
        imask = phase_name_assem==iphsnm
        iwt = wt_stable[imask]
        # icomp_stable = np.sum(comp_assem[imask,:], axis=0)
        inmol_elems = np.dot(iwt, comp_assem[imask])
        # iX = extras['X'][iphsnm]
        # inmol_endmem = iX
        #
        #
        # imask_phs = phase_name_curr==iphsnm
        # print(iX)
        # print(imask_phs)

        iphscomp = extras['phase_comps'][iphsnm]

        # imask_all =
        # print(iphsnm)

        output = np.linalg.lstsq(iphscomp.T, inmol_elems, rcond=None)
        inmol_endmem = output[0]


        # print(imask)
        # print('nmol elems = ', inmol_stable)
        # print('nmol endmem = ', inmol_endmem)
        # print('dev = ', np.dot(iphscomp.T, inmol_endmem)-inmol_stable)
        # print('iphscomp = ',iphscomp)
        # print('tot = ', np.sum(inmol_stable))
        # print('----')
        nmol_elems[ind] = inmol_elems
        nmol_stable.append(inmol_endmem)
        comp_endmem_stable.append(iphscomp)

    nmol_stable = np.hstack(nmol_stable)
    comp_endmem_stable = np.vstack(comp_endmem_stable)

    # print(phase_stable)
    # print(nmol_stable)
    # print(comp_endmem_stable)

    extras['chempot'] = chempot
    extras['wt_assem'] = wt_assem
    extras['Aff_soln'] = Aff_soln
    extras['Aff_endmem'] = Aff_endmem
    extras['phase_name_parent'] = phase_name_parent
    extras['phase_endmem_ids'] = phase_name_curr

    return phase_stable, nmol_stable, comp_endmem_stable, extras






def linear_step(ycurr, rnorm_curr, yscl, bulk_comp, xy, step=.03):
    #bnds[0]*(1-step)+bnds[1]*step
    xy_bnd_step = np.hstack((bulk_comp, ycurr + step*yscl))
    wt_step, rnorm_step = opt.nnls(xy.T, xy_bnd_step)

    rnorm_grad = (rnorm_curr-rnorm_step)/step
    df = rnorm_curr/rnorm_grad

    ynext = ycurr + df*yscl
    # imu = bnds[0]*(1-df)+bnds[1]*df
    # ixy_bnd = np.hstack((bulk_comp, imu))
    xy_bnd_next = np.hstack((bulk_comp, ynext))
    wt_next, rnorm_next = opt.nnls(xy.T, xy_bnd_next)
    yscl_next = rnorm_next/rnorm_grad

    return ynext, rnorm_next, yscl_next, wt_next


# +
comp = comp_endmem
TOLmu=10


def min_assemblage_samp(bulk_comp, comp, mu, chempot=None, sample=True, N=1001):
    if chempot is None:
        output = np.linalg.lstsq(comp, mu, rcond=None)
        chempot = output[0]

    if np.isscalar(chempot) and chempot==0:
        chempot = np.zeros(len(bulk_comp))
    # print(chempot)

    mu0_bulk = np.dot(chempot, bulk_comp)
    mu_mod = np.dot(chempot, comp.T)
    dmu = mu-mu_mod
    ind_min = np.argmin(dmu)
    mu_shft = dmu[ind_min]
    # mu_shft = -5000

    bnds = np.array([mu_shft, 0]) + mu0_bulk

    xy = np.hstack((comp, mu[:,np.newaxis]))

    # xy_bnd_hi = np.hstack((bulk_comp, bnds[1]))
    # wt_hi, rnorm_hi = opt.nnls(xy.T, xy_bnd_hi)

    if sample:
        # mu_vals = np.linspace(bnds[0],bnds[1],N)
        mu_shft = np.min((mu_shft,-1000))
        mu_vals = np.linspace(mu0_bulk+4*mu_shft,mu0_bulk-3*mu_shft,N)
        wt = np.zeros((N, len(mu)))
        rnorm = np.zeros(N)
        chempot = np.zeros((N, comp.shape[1]))
        for ind,imu in enumerate(mu_vals):
            ixy_bnd = np.hstack((bulk_comp, imu))
            iwt, irnorm = opt.nnls(xy.T, ixy_bnd)

            icomp_assem = comp[iwt>0,:]
            imu_assem = mu[iwt>0]
            ioutput = np.linalg.lstsq(icomp_assem, imu_assem, rcond=None)
            ichempot = ioutput[0]

            wt[ind] = iwt
            rnorm[ind] = irnorm
            chempot[ind,:] = ichempot

        return chempot, mu_vals, rnorm, None, wt

    mu_init = bnds[0]
    xy_bnd_init = np.hstack((bulk_comp, mu_init))
    wt_init, rnorm_init = opt.nnls(xy.T, xy_bnd_init)


    mu_scl = bnds[1]-bnds[0]
    mu_next, rnorm_next, mu_scl_next, wt_next = linear_step(
        mu_init, rnorm_init, mu_scl, bulk_comp, xy, step=.03)


    comp_assem = comp[wt_next>0,:]
    mu_assem = mu[wt_next>0]
    output = np.linalg.lstsq(comp_assem, mu_assem, rcond=None)
    chempot_next = output[0]

    return chempot_next, mu_next, rnorm_next, mu_scl_next, wt_next






# +
def min_assemblage(bulk_comp, comp, mu, mu_shft=None, chempot=None):

    if chempot is None:
        output = np.linalg.lstsq(comp, mu, rcond=None)
        chempot = output[0]

    if np.isscalar(chempot) and chempot==0:
        chempot = np.zeros(len(bulk_comp))


    mu0_bulk = np.dot(chempot, bulk_comp)
    mu_mod = np.dot(chempot, comp.T)
    Aff = mu-mu_mod
    print(Aff)

    if mu_shft is None:
        mu_shft = np.min(Aff)

    print('mu_shft = ', mu_shft)
    mu_bnd = mu0_bulk+mu_shft
    # might need to bump up

    # ind_min = np.argmin(dmu)
    # bnds = np.array([dmu[ind_min], 0]) + mu0_bulk

    xy = np.hstack((comp, mu[:,np.newaxis]))

    xy_bnd = np.hstack((bulk_comp, mu_bnd))
    wt_bnd, rnorm_bnd = opt.nnls(xy.T, xy_bnd)
    print('wt_bnd = ', wt_bnd)

    ind_assem = np.where(wt_bnd>0)[0]
    comp_assem_sub = comp[ind_assem,:]
    mu_assem_sub = mu[ind_assem]

    output = np.linalg.lstsq(comp_assem_sub, mu_assem_sub, rcond=None)
    chempot_assem = output[0]

    wt_assem = np.zeros(wt_bnd.size)
    wt_assem_sub, rnorm_assem = opt.nnls(comp_assem_sub.T, bulk_comp)
    mu_assem = np.dot(wt_assem_sub, mu_assem_sub)

    wt_assem[ind_assem] = wt_assem_sub


    return chempot_assem, mu_assem, rnorm_assem, wt_assem





# +
def find_min_assemblage(bulk_comp, comp, mu, mu_shft=None, chempot=None, mu_shft_bnd=-100, debug=False):

    if chempot is None:
        output = np.linalg.lstsq(comp, mu, rcond=None)
        chempot = output[0]

    if np.isscalar(chempot) and chempot==0:
        chempot = np.zeros(len(bulk_comp))


    bulk_mu0_avg = np.dot(chempot, bulk_comp)
    mu_mod = np.dot(chempot, comp.T)
    Aff = mu-mu_mod

    if mu_shft is None:
        mu_shft = np.min([np.min(Aff),-mu_shft_bnd])

    bulk_mu0 = bulk_mu0_avg+mu_shft
    # might need to bump up

    # ind_min = np.argmin(dmu)
    # bnds = np.array([dmu[ind_min], 0]) + mu0_bulk

    bulk_mu, wt_hull, rnorm_hull=  find_local_convex_hull(bulk_comp, bulk_mu0, comp, mu)
    if debug:
        print('rnorm_hull = ',rnorm_hull)

    chempot_assem, Gtot, wt_assem, rnorm_assem = fit_chempot_constraints(bulk_comp, wt_hull, comp, mu)

    return chempot_assem, Gtot, wt_assem, rnorm_assem

def fit_chempot_constraints(bulk_comp, wt_hull, comp, mu, debug=False):
    ind_assem = np.where(wt_hull>0)[0]
    comp_assem_sub = comp[ind_assem,:]
    mu_assem_sub = mu[ind_assem]

    # Add additional constraints on chempot here!
    output = np.linalg.lstsq(comp_assem_sub, mu_assem_sub, rcond=None)
    chempot = output[0]

    # Add additional constraints on chempot here also!
    wt_assem = np.zeros(mu.size)
    wt_assem_sub, rnorm_assem = opt.nnls(comp_assem_sub.T, bulk_comp)

    # This is more general to apply to chempot (non-bulk-comp) constraints
    Gtot = np.dot(wt_assem_sub, mu_assem_sub)
    Gtot_chempot = np.dot(chempot, bulk_comp)

    # This check only possible if full bulk comp is provided
    if debug and np.abs(Gtot_chempot-Gtot)>1e-3:
        print('****  delG = ', Gtot_chempot-Gtot, '  *****')
        # assert False, 'Total Gibbs energy does not agree between methods'

    # print(Gtot_chempot-Gtot)

    wt_assem[ind_assem] = wt_assem_sub
    return chempot, Gtot, wt_assem, rnorm_assem

def find_local_convex_hull(bulk_comp, bulk_mu0, comp, mu, mu_step=3, rnorm_TOL=1e-6, debug=False):
    """
    Find phases on local part of convex hull (at chosen bulk comp)
    """

    xy = np.hstack((comp, mu[:,np.newaxis]))
    # print('INIT: bulk_mu = ', bulk_mu0)

    bulk_mu = bulk_mu0
    xy_bulk = np.hstack((bulk_comp, bulk_mu))
    wt, rnorm = opt.nnls(xy.T, xy_bulk)

    wt_comp, rnorm_lim = optimize.nnls(comp.T, bulk_comp)
    if debug:
        print('rnorm_lim = ', rnorm_lim)

    def newton_solve(rnorm, xy_bulk, xy=xy, mu_step=mu_step, rnorm_lim=rnorm_lim):
        bulk_mu = xy_bulk[-1]
        xy_bulk[-1] = bulk_mu-mu_step

        wt_step, rnorm_step = opt.nnls(xy.T, xy_bulk)
        drnorm = rnorm_step-rnorm
        grad = -drnorm/mu_step

        bulk_mu_zero = bulk_mu-(rnorm-rnorm_lim)/grad
        xy_bulk[-1] = bulk_mu_zero

        wt_zero, rnorm_zero = opt.nnls(xy.T, xy_bulk)
        return bulk_mu_zero, wt_zero, rnorm_zero, grad

    rnorm_prev = rnorm
    bulk_mu_prev = bulk_mu

    while True:
        # print('rnorm = ', rnorm)
        # print('bulk_mu = ', bulk_mu )
        xy_bulk[-1] = bulk_mu_prev
        bulk_mu, wt, rnorm, grad = newton_solve(rnorm_prev, xy_bulk)
        # print('grad = ', grad)
        mu_step = np.abs(rnorm_prev/grad)


        while rnorm==rnorm_lim:
            # print('rnorm0 ...')
            bulk_mu -= 0.1*mu_step
            # mu_step *= 0.9
            # print('bulk_mu = ', bulk_mu)
            xy_bulk[-1] = bulk_mu
            wt, rnorm = opt.nnls(xy.T, xy_bulk)
            # print('rnorm = ', rnorm)

        rnorm_prev = rnorm
        bulk_mu_prev = bulk_mu
        if rnorm - rnorm_lim< rnorm_TOL:
            break

    return bulk_mu, wt, rnorm



# -











# +
# wt_assem0
# -



phases = get_subsolidus_phases(database=database)
phs_sym, endmem_ids, mu_endmem, comp_endmem, sys_elems, phase_comps = (
    system_energy_landscape(T, P, phases, prune_polymorphs=False))



# +
# plt.figure()
# plt.plot((approx_mu_phases(wt_assem0)-mu)/1e3,'ko')
# plt.plot((approx_mu_phases(nelem0)-mu)/1e3,'rx')
# -

def equilibrium_assemblage(T, P, bulk_comp, phases,
                           comp_endmem, mu_endmem, GTOL=0.1):
    # This options should slightly speed calculation,
    #  but is currently broken

    rank = np.linalg.matrix_rank(comp_endmem)
    # print('rank = ', rank)

    # Initialize with endmembers
    chempot_assem0, Gtot0, wt_assem0, rnorm_assem = find_min_assemblage(
        bulk_comp, comp_endmem, mu_endmem, chempot=None)
    # print('Gtot = ', Gtot0)
    chempot_assem = chempot_assem0

    mu_soln0, dmudn_soln0, comp_soln0, X0, A0, natom,phase_names =  (
        phase_affinity(chempot_assem0, phases, phase_comps, T, P))
    Aff_soln0_a = np.array(list(A0.values()))
    comp_soln0_a = np.array(list(comp_soln0.values()))

    Aff_soln_a = Aff_soln0_a.copy()
    comp_soln_a = comp_soln0_a.copy()


    inds = np.argsort(Aff_soln_a)
    print('phs_names [sort] = ', phase_names[inds])
    print('Aff_soln [sort] = ', Aff_soln_a[inds])
    # print('Aff_soln = ', Aff_soln_a)
    # print('comp_soln = ', comp_soln_a)
    print('=============')

    while True:
        try:
            dchempot_assem, Gtot, wt_assem, rnorm_assem = find_min_assemblage(
                bulk_comp, comp_soln_a, Aff_soln_a, chempot=0)
        except:
            chempot_next, mu_next, rnorm_next, mu_scl_next, wt_next = min_assemblage_samp(
                bulk_comp, comp_soln_a, Aff_soln_a, chempot=0, sample=True)

            plt.figure()
            plt.plot(mu_next, rnorm_next,'-');

            plt.figure()
            plt.plot(mu_next, wt_next,'-');
            print('FAIL')
            break


        # print('dchempot = ', dchempot_assem)
        # print('   Gtot = ', Gtot)

        Aff_assem = Aff_soln_a[wt_assem>0]
        if (Gtot < GTOL and np.all(dchempot_assem < GTOL) and
            np.all(np.abs(Aff_assem)<GTOL)):
            break


        # chempot_assem = chempot_assem0+dchempot_assem
        chempot_assem += dchempot_assem

        mu_soln, dmudn_soln, comp_soln, X, A, natom,phase_names =  (
            phase_affinity(chempot_assem, phases, phase_comps, T, P))
        Aff_soln_a = np.array(list(A.values()))
        comp_soln_a = np.array(list(comp_soln.values()))

        print('dchempot = ', dchempot_assem)
        inds = np.argsort(Aff_soln_a)
        print('phs_names [sort] = ', phase_names[inds])
        print('Aff_soln [sort] = ', Aff_soln_a[inds])
        # print('Aff_soln [sort] = ', np.sort(Aff_soln_a))
        # print('comp_soln = ', comp_soln_a)
        print('=============')


    return chempot_assem, Aff_soln_a, comp_soln_a

# +
# wt = np.random.rand(comp_endmem.shape[0])
# wt = wt/np.sum(wt)
# bulk_comp = np.dot(wt, comp_endmem)

# +
# comp_endmem.shape
# -

# # Get bulk comp appropriate to lower mantle

# +


# phs_assem = ['PrvS','AkiS','CfS','Sti','CaPrv','Fp']
phs_assem = ['PrvS','CaPrv','Fp','AkiS','CfS']
# phs_assem

mask = np.sum([phs_sym==iphs for iphs in phs_assem],axis=0)>0
phs_sym[mask]
# -

wt = np.random.rand(comp_endmem.shape[0])
wt[~mask] = 0
wt = wt/np.sum(wt)
bulk_comp = np.dot(wt, comp_endmem)



# +
# # %timeit
# chempot_assem, Aff_soln_a, comp_soln_a = equilibrium_assemblage(
#     T, P, bulk_comp, phases, comp_endmem, mu_endmem)
# -



rank = np.linalg.matrix_rank(comp)
rank


# +
def get_phase_endmem_sym(phs_sym, endmem_ids):
    phs_endmem_sym = []
    for iphs,id_num in zip(phs_sym,endmem_ids.astype(dtype=str)):
        iphsid = iphs+'_'+id_num
        phs_endmem_sym.append(iphsid)

    phs_endmem_sym = np.array(phs_endmem_sym)
    return phs_endmem_sym


# -



# +
# comp_curr*wt_assem[:,np.newaxis]

# +
# assem_phases = np.unique(phase_name_parent[wt_assem>0])
# for iphsnm in assem_phases:
#     iphs = phases[iphsnm]
#     phase_name_parent
#     imask = phase_name_parent==iphsnm
#     icomp = np.dot(comp_curr[imask].T,wt_assem[imask])
#     print(iphs)
#     print(icomp)
#     print('===')
# -



# +


# +
# comp_endmem.shape
# -



# +
def get_current_phase_mu(Aff_soln, Aff_endmem, comp_soln, comp_endmem=comp_endmem,
                         phs_sym=phs_sym, Ashft=1e3, phases=phases):

    # mu_endmem_curr = np.dot(comp_endmem, chempot_init)
    Nendmem = comp_endmem.shape[0]
    dmu_endmem = np.zeros(Nendmem)

    # for iphs in phase_aff:
    #     iA = phase_aff[iphs]
    #     imask = phs_sym==iphs
    #     dmu_endmem[imask] = iA+Ashft

    dmu_endmem = Aff_endmem + Ashft


    dmu_curr = np.hstack((Aff_soln,dmu_endmem))

    comp_curr = np.vstack((comp_soln, comp_endmem))
    # phase_name_curr = np.hstack((phase_names, phs_endmem_sym))
    # phase_parent_curr = np.hstack((phase_names, phs_sym))
    #
    # phase_names = phase_name_curr
    # soln_phase_names = phase_parent_curr

    # return comp_curr, dmu_curr, phase_names, soln_phase_names
    return comp_curr, dmu_curr



# -



# +
# # %%timeit
# linear only method with vertical offset, with update
# IN PROGRESS!!!

# Nphs = len(phases)
# Nphs

# -






def build_stable_linear_phase_model(phase_stable, nmol_stable, T, P, phases):

    Nendmem_stable = nmol_stable.size
    mu0_stable = np.zeros(Nendmem_stable)
    dmudn_stable = np.zeros((Nendmem_stable,Nendmem_stable))
    phase_endmem_num_stable = []

    ind = 0
    for iphsnm in phase_stable:
        iphs = phases[iphsnm]
        iendmem_num = iphs.endmember_num
        inmol = nmol_stable[ind:ind+iendmem_num]

        elem_comp = iphs.props['element_comp']
        inelem = np.sum(elem_comp,axis=1)

        phase_endmem_num_stable.append(inmol.size)
        # print(iphsnm)
        # print(iendmem_num)
        # print(inmol_stable)

        if inmol.size==1:
            iX = None
        else:
            iX = inmol.copy()

        imu0 = iphs.chem_potential(T, P, mol=iX).squeeze()/inelem

        # print(inelem)
        # print(imu0)
        # print(iphs.chem_potential(T, P, mol=iX).squeeze())
        # imu0 = iphs.gibbs_energy(T, P, mol=inmol_stable, derivs={'dmol':1})

        if iX is None:
            idmudn = np.array([[0]])
        else:
            idmudn = np.squeeze(iphs.gibbs_energy(
                T, P, mol=iX, deriv={'dmol':2}))/inelem


        # print(idmudn)
        # print('---')

        mu0_stable[ind:ind+iendmem_num] = imu0
        dmudn_stable[ind:ind+iendmem_num,
                     ind:ind+iendmem_num] = idmudn

        ind += iendmem_num

    phase_endmem_num_stable = np.array(phase_endmem_num_stable)

    return mu0_stable, dmudn_stable, phase_endmem_num_stable









# +
# # %load_ext snakeviz

# +


# Failure of affinity and comp method
bulk_comp_fail =  np.array([0.57923856, 0.01926474, 0.06734043, 0.13956022, 0.09832938,
       0.03137229, 0.06489438])
# -







# +
def get_affinity_wt(Aff_phases, phase_endmem_num, Ascl_thresh, Aff_cutoff=300):

    Ascl = np.abs(np.median(Aff_phases[Aff_phases<=0]))
    Ascl = np.max((Ascl, Ascl_thresh))

    Amag = Aff_phases/Ascl
    # Amask = Amag>Aff_cutoff
    Amask = Aff_phases>Aff_cutoff

    wtstdt = (1+1/5*Amag[Amask]**2)**(-(5+1)/2)
    # wtcauchy = 1/(1+Amag[Amask]**2)

    Affwt = np.ones(Amag.size)
    Affwt[Amask] = wtstdt
    # Affwt[Amask] = 0

    Affwt_endmem = np.repeat(Affwt, phase_endmem_num)
    # print('Aff_phases = ', Aff_phases)
    # print('Affwt = ', Affwt)
    # print('Affwt_endmem = ', Affwt_endmem)
    return Affwt_endmem, Affwt



def get_linear_constraints(bulk_comp, Gtot, mu0_endmem, nmol_end0, dmudn_tot,
                           phase_endmem_num, comp_endmem, apply_nmol0_prior=False):
    Npure = np.sum(phase_endmem_num==1)
    Nendmem = phase_endmem_num.sum()
    Nelems = comp_endmem.shape[1]
    Nparams = Nelems+Nendmem
    Nsoln_endmem = Nendmem-Npure

    mask_pure = np.tile(False, Nendmem)
    mask_pure[-Npure:] = True

    A_mu_pure = np.zeros((Npure, Nparams))
    A_mu_pure[:,:Nelems] = comp_endmem[mask_pure,:]
    B_mu_pure = mu0_endmem[mask_pure].copy()

    # NOTE: this relies on the pure elements being last!!!!
    A_mu_soln = np.zeros((Nsoln_endmem, Nparams))
    A_mu_soln[:,:Nelems] = comp_endmem[~mask_pure,:]
    A_mu_soln[:,Nelems:Nelems+Nsoln_endmem] = -dmudn_tot[:-Npure,:-Npure]
    B_mu_soln = mu0_endmem[:-Npure] - np.dot(dmudn_tot[:-Npure,:-Npure], nmol_end0[:-Npure])

    A_comp = np.zeros((Nelems, Nparams))
    A_comp[:, -Nendmem:] = comp_endmem.T
    B_comp = bulk_comp.copy()

    # A_mu_comp = np.zeros((Nsoln_endmem, Nparams))
    # A_mu_comp[:,:Nelems] = -np.dot(dndmu_tot[:-Npure,:-Npure], comp_endmem[:-Npure, :])
    # A_mu_comp[:,Nelems:Nelems+Nsoln_endmem] = np.eye(Nsoln_endmem)
    # B_mu_comp = np.zeros(Nsoln_endmem)
    # B_mu_comp = nmol_end0[:-Npure] - np.dot(dndmu_tot[:-Npure,:-Npure], mu0_endmem[:-Npure])

    A_nmol_prior = np.zeros((Nendmem, Nparams))
    A_nmol_prior[:,Nelems:] = np.eye(Nendmem)
    if apply_nmol0_prior:
        B_nmol_prior = nmol_end0
    else:
        B_nmol_prior = np.zeros(Nendmem)


    A_mu_prior = np.zeros((1,Nparams))
    A_mu_prior[0,0] = 1
    B_mu_prior = np.zeros(1)

    A_Gtot = np.zeros((1, Nparams))
    A_Gtot[0,:Nelems] = bulk_comp
    B_Gtot = Gtot

    A = {}
    A['mu_pure'] = A_mu_pure
    A['mu_soln'] = A_mu_soln
    A['comp']    = A_comp
    A['nmol_prior'] = A_nmol_prior
    A['mu_prior'] = A_mu_prior
    A['Gtot'] = A_Gtot

    B = {}
    B['mu_pure'] = B_mu_pure
    B['mu_soln'] = B_mu_soln
    B['comp']    = B_comp
    B['nmol_prior'] = B_nmol_prior
    B['mu_prior'] = B_mu_prior
    B['Gtot'] = B_Gtot

    return A, B

def build_lstsq(A, B, Affwt_endmem, phase_endmem_num, sig_nmol_prior=.2, sig_comp=.01, sig_mu=10, sig_Gtot=1e-3):
    Npure = np.sum(phase_endmem_num==1)
    Affwt_soln = Affwt_endmem[:-Npure]
    Affwt_pure = Affwt_endmem[-Npure:]

    sig_mu_prior = sig_mu*3


    # # single constraints on bulk_comp, mu_pure, mu_soln
    # A_lsq = np.vstack((A['mu_pure']*Affwt_pure[:,np.newaxis]/sig_mu,
    #                    A['mu_soln']*Affwt_soln[:,np.newaxis]/sig_mu,
    #                    A['comp']/sig_comp,
    #                    A['Gtot']/sig_Gtot))
    #
    # B_lsq = np.hstack((B['mu_pure']*Affwt_pure/sig_mu,
    #                    B['mu_soln']*Affwt_soln/sig_mu,
    #                    B['comp']/sig_comp,
    #                    B['Gtot']/sig_Gtot))

    Nendmem = Affwt_endmem.size
    Affcompwt = np.zeros(A['comp'].shape[1])
    Affcompwt[-Nendmem:] = Affwt_endmem

    # single constraints on bulk_comp, mu_pure, mu_soln
    A_lsq = np.vstack((A['mu_pure']*Affwt_pure[:,np.newaxis]/sig_mu,
                       A['mu_soln']*Affwt_soln[:,np.newaxis]/sig_mu,
                       A['comp']*Affcompwt[np.newaxis,:]/sig_comp,
                       A['nmol_prior']*Affwt_endmem[:, np.newaxis]/sig_nmol_prior,
                       A['mu_prior']/sig_mu_prior))


    B_lsq = np.hstack((B['mu_pure']*Affwt_pure/sig_mu,
                       B['mu_soln']*Affwt_soln/sig_mu,
                       B['comp']/sig_comp,
                       B['nmol_prior']*Affwt_endmem/sig_nmol_prior,
                       B['mu_prior']/sig_mu_prior))

    # # single constraints on bulk_comp, mu_pure, mu_soln
    # A_lsq = np.vstack((A['mu_pure']*Affwt_pure[:,np.newaxis]/sig_mu,
    #                    A['mu_soln']*Affwt_soln[:,np.newaxis]/sig_mu,
    #                    A['comp']*Affcompwt[np.newaxis,:]/sig_comp,
    #                    A['mu_prior']/sig_mu_prior))
    #
    #
    # B_lsq = np.hstack((B['mu_pure']*Affwt_pure/sig_mu,
    #                    B['mu_soln']*Affwt_soln/sig_mu,
    #                    B['comp']/sig_comp,
    #                    B['mu_prior']/sig_mu_prior))


    if ~np.isnan(B['Gtot']):
        A_lsq = np.vstack((A_lsq, A['Gtot']/sig_Gtot))
        B_lsq = np.hstack((B_lsq, B['Gtot']/sig_Gtot))

    # A_lsq = np.vstack((A['mu_pure']*Affwt_pure[:,np.newaxis]/sig_mu,
    #                    A['mu_soln']*Affwt_soln[:,np.newaxis]/sig_mu,
    #                    A['comp']/sig_comp,
    #                    A['nmol_prior']/sig_nmol_prior,
    #                    A['mu_prior']/sig_mu_prior))
    #
    # B_lsq = np.hstack((B['mu_pure']*Affwt_pure/sig_mu,
    #                    B['mu_soln']*Affwt_soln/sig_mu,
    #                    B['comp']/sig_comp,
    #                    B['nmol_prior']/sig_nmol_prior,
    #                    B['mu_prior']/sig_mu_prior))
    return A_lsq, B_lsq


def update_fitted_affinities(params, A, B, phase_endmem_num):
    Nendmem = phase_endmem_num.sum()
    nmols_fit = params[-Nendmem:]

    dmu_soln_fit = B['mu_soln'] - np.dot(A['mu_soln'], params)
    dmu_pure_fit = B['mu_pure'] - np.dot(A['mu_pure'], params)
    dmu_fit = np.hstack((dmu_soln_fit, dmu_pure_fit))

    ind_curr=0
    Aff_phases_fit = []
    for iendmem_num in phase_endmem_num:
        idmu = dmu_fit[ind_curr:ind_curr+iendmem_num]
        inmol = nmols_fit[ind_curr:ind_curr+iendmem_num]

        iAff = np.dot(inmol/np.sum(inmol), idmu)
        Aff_phases_fit.append(iAff)

        ind_curr += iendmem_num

    Aff_phases_fit = np.array(Aff_phases_fit)
    return Aff_phases_fit
# -











# ## minimize assemblage

def equil_min_assemblage(bulk_comp, nmol_stable, mu0_stable, dmudn_stable,
                         comp_endmem_stable, phase_endmem_num_stable,
                         sig_nmol_prior=0.1, sig_mu=10, sig_comp=0.01, molTOL=1e-4):

    Gtot_init = np.nan

    while True:
        A, B = get_linear_constraints(bulk_comp, Gtot_init, mu0_stable, nmol_stable, dmudn_stable,
                                      phase_endmem_num_stable, comp_endmem_stable, apply_nmol0_prior=True)


        wt_stable = np.ones(phase_endmem_num_stable.sum())
        A_lsq, B_lsq = build_lstsq(A, B, wt_stable, phase_endmem_num_stable,
                                   sig_nmol_prior=sig_nmol_prior, sig_comp=sig_comp, sig_mu=sig_mu)

        output = np.linalg.lstsq(A_lsq, B_lsq, rcond=None)
        params = output[0]
        chempot_fit = params[:Nelems]
        nmol_fit = params[Nelems:]

        resid = np.dot(A_lsq, params)-B_lsq
        chisqr = np.sqrt(np.mean(resid**2))

        dnmol = nmol_fit - nmol_stable
        mu_fit = mu0_stable + np.dot(dmudn_stable, dnmol)

        # print('chisqr = ', chisqr)
        # print('dcomp = ', np.dot(A['comp'],params)-B['comp'])
        # print('dmu(pure) = ', np.dot(A['mu_pure'],params)-B['mu_pure'])
        # print('dmu(soln) = ', np.dot(A['mu_soln'],params)-B['mu_soln'])


        mu0_stable_next, dmudn_stable_next, phase_endmem_num_stable = build_stable_linear_phase_model(
            phase_stable, nmol_fit, T, P, phases)


        # extras['mu0']+np.dot(extras['dmudn_stable'],nmol_stable - extras['nmol0_stable']) - mu_stable_fin
        # dmu_corr = mu0_stable + np.dot(dmudn_stable,nmol_fit) - mu0_stable_next
        dmu_corr = mu0_stable_next-mu_fit
        # print(dmu_corr)


        nmol0_stable = nmol_stable.copy()

        chempot_stable = chempot_fit
        nmol_stable = nmol_fit
        mu0_stable = mu0_stable_next
        dmudn_stable = dmudn_stable_next

        if np.all(np.abs(dnmol)<molTOL):
            break


    extras = {}
    extras['mu_fit'] = mu_fit
    extras['mu0'] = mu0_stable
    extras['nmol0_stable'] = nmol0_stable
    extras['dmudn_stable'] = dmudn_stable
    extras['chisqr'] = chisqr

    return chempot_stable, nmol_stable, extras



# +

phs_assem = ['PrvS','CaPrv','Fp','AkiS','CfS']
mask = np.sum([phs_sym==iphs for iphs in phs_assem],axis=0)>0

wt = np.random.rand(comp_endmem.shape[0])
wt[~mask] = 0
wt = wt/np.sum(wt)
bulk_comp = np.dot(wt, comp_endmem)

# +

# phase_stable, nmol0_stable, comp_endmem_stable, extras = get_init_assem(bulk_comp, comp_endmem, mu_endmem)
# -



# +
phase_stable, nmol0_stable, comp_endmem_stable, extras = get_init_assem(bulk_comp, comp_endmem, mu_endmem)


mu0_stable, dmudn_stable, phase_endmem_num_stable = build_stable_linear_phase_model(phase_stable, nmol0_stable, T, P, phases)
nmol_init_stable = nmol0_stable.copy()
nmol_stable = nmol0_stable.copy()
print(phase_endmem_num_stable)
np.dot(comp_endmem_stable.T, nmol_stable)-bulk_comp
# -

extras.keys()

# +
inds = np.argsort(Aff_soln)
phase_names_soln[inds]

phase_assem = []
inds_curr = 0
rank_curr = 0

while True:
    # print(phase_assem)
    iphase_assem = phase_assem.copy()
    iphase_assem.append(phase_names_soln[inds[inds_curr]])
    [phase_comps[iphsnm] for iphsnm in phase_assem]
    np.linalg.matrix_rank(np.vstack())
    inds_curr += 1
# -



np.linalg.matrix_rank()

print('nmol0 = ', nmol0_stable)
print('phase_endmem_num_stable = ', phase_endmem_num_stable)

# +

chempot_stable, nmol_stable, extras = equil_min_assemblage(
    bulk_comp, nmol_stable, mu0_stable, dmudn_stable, comp_endmem_stable, phase_endmem_num_stable,
    sig_nmol_prior=0.1, molTOL=1e-6)
# -



np.dot(comp_endmem_stable, chempot_stable)-extras['mu0']



extras['mu0'] - extras['mu_fit']

np.dot(comp_endmem_stable.T, nmol_stable)-bulk_comp

comp_soln, X_soln, Aff_soln, Aff_endmem, phase_names_soln, extras_aff = update_phase_affinities(chempot_stable)


# +
# phase_names_soln
mask_stable = np.isin(phase_names_soln, phase_stable)
print(Aff_soln[mask_stable])
print(phase_names_soln[Aff_soln<1])

# why are these not all 0 within tolerance?
# -



# +

phase_stable, nmol0_stable, comp_endmem_stable, extras = get_init_assem(
    bulk_comp, comp_endmem, mu_endmem,chempot_init=chempot_stable)

nmol0_stable=np.abs(nmol0_stable)

mu0_stable, dmudn_stable, phase_endmem_num_stable = build_stable_linear_phase_model(phase_stable, nmol0_stable, T, P, phases)
nmol_init_stable = nmol0_stable.copy()
nmol_stable = nmol0_stable.copy()

# -





# +

chempot_stable, nmol_stable, extras = equil_min_assemblage(
    bulk_comp, nmol_stable, mu0_stable, dmudn_stable, comp_endmem_stable, phase_endmem_num_stable,
    sig_nmol_prior=0.1, molTOL=1e-6)
# -







# +
phase_neg_aff = phase_names_soln[Aff_soln < 1]
mask_new = np.array([iphs not in phase_stable for iphs in phase_neg_aff])
new_phases = phase_neg_aff[mask_new]

new_endmem_num = []
comp_endmem_new = []
for iphsnm in new_phases:
    iphs = phases[iphsnm]
    iendmem_num = iphs.endmember_num
    iphs_comp = phase_comps[iphsnm]

    print(iphs_comp)

    new_endmem_num.append(iendmem_num)
    comp_endmem_new.append(iphs_comp)

comp_endmem_new.append(comp_endmem_stable)

comp_endmem_new = np.vstack(comp_endmem_new)
phase_new_stable = np.hstack((new_phases, phase_stable))

new_endmem_num = np.array(new_endmem_num)
new_nmol = 1e-2*np.ones(new_endmem_num.sum())
# comp_endmem_new = np.vstack((comp_endmem_stable, comp_endmem_new))

# phase_endmem_num_stable_new = np.hstack((phase_endmem_num_stable, new_endmem_num))
nmol_stable_new = np.hstack((new_nmol, nmol_stable))
# -



# +

# mu0_stable, dmudn_stable, phase_endmem_num_stable = build_stable_linear_phase_model(phase_stable, nmol0_stable, T, P, phases)
# nmol_init_stable = nmol0_stable.copy()
# nmol_stable = nmol0_stable.copy()
#
# np.dot(comp_endmem_stable.T, nmol_stable)-bulk_comp
# -

mu1_stable, dmudn1_stable, phase_endmem_num1_stable = build_stable_linear_phase_model(
    phase_new_stable, nmol_stable_new, T, P, phases)


phase_endmem_num1_stable

# +

chempot_stable, nmol_stable, extras = equil_min_assemblage(
    bulk_comp, nmol_stable_new, mu1_stable, dmudn1_stable, comp_endmem_new, phase_endmem_num1_stable,
    sig_nmol_prior=0.1, molTOL=1e-6)
# -



def alt_equil_min_assemblage(bulk_comp, nmol_stable, mu0_stable, dmudn_stable,
                         comp_endmem_stable, phase_endmem_num_stable,
                         sig_nmol_prior=0.1, sig_mu=10, sig_comp=0.01, molTOL=1e-4):

    Gtot_init = np.nan

    while True:
        A, B = get_linear_constraints(bulk_comp, Gtot_init, mu0_stable, nmol_stable, dmudn_stable,
                                      phase_endmem_num_stable, comp_endmem_stable, apply_nmol0_prior=True)


        wt_stable = np.ones(phase_endmem_num_stable.sum())
        A_lsq, B_lsq = build_lstsq(A, B, wt_stable, phase_endmem_num_stable,
                                   sig_nmol_prior=sig_nmol_prior, sig_comp=sig_comp, sig_mu=sig_mu)

        output = np.linalg.lstsq(A_lsq, B_lsq, rcond=None)
        params = output[0]
        chempot_fit = params[:Nelems]
        nmol_fit = params[Nelems:]

        resid = np.dot(A_lsq, params)-B_lsq
        chisqr = np.sqrt(np.mean(resid**2))

        dnmol = nmol_fit - nmol_stable
        mu_fit = mu0_stable + np.dot(dmudn_stable, dnmol)

        # print('chisqr = ', chisqr)
        # print('dcomp = ', np.dot(A['comp'],params)-B['comp'])
        # print('dmu(pure) = ', np.dot(A['mu_pure'],params)-B['mu_pure'])
        # print('dmu(soln) = ', np.dot(A['mu_soln'],params)-B['mu_soln'])


        mu0_stable_next, dmudn_stable_next, phase_endmem_num_stable = build_stable_linear_phase_model(
            phase_stable, nmol_fit, T, P, phases)


        # extras['mu0']+np.dot(extras['dmudn_stable'],nmol_stable - extras['nmol0_stable']) - mu_stable_fin
        # dmu_corr = mu0_stable + np.dot(dmudn_stable,nmol_fit) - mu0_stable_next
        dmu_corr = mu0_stable_next-mu_fit
        # print(dmu_corr)


        nmol0_stable = nmol_stable.copy()

        chempot_stable = chempot_fit
        nmol_stable = nmol_fit
        mu0_stable = mu0_stable_next
        dmudn_stable = dmudn_stable_next

        if np.all(np.abs(dnmol)<molTOL):
            break


    extras = {}
    extras['mu_fit'] = mu_fit
    extras['mu0'] = mu0_stable
    extras['nmol0_stable'] = nmol0_stable
    extras['dmudn_stable'] = dmudn_stable
    extras['chisqr'] = chisqr

    return chempot_stable, nmol_stable, extras

phase_endmem_num

phase_stable_new = np.hstack((phase_stable, new_phases))

# +

mu0_stable, dmudn_stable, phase_endmem_num_stable = build_stable_linear_phase_model(phase_stable, nmol0_stable, T, P, phases)
# -







# +
X_eq = np.hstack([X_soln[iphs] for iphs in phase_stable])
X_eq

ind = 0
for iendmem_num in phase_endmem_num_stable:
    iX = X_eq[ind:ind+iendmem_num]
    inmol = nmol_stable[ind:ind+iendmem_num]
    iX_fit = inmol/np.sum(inmol)
    print(iX-iX_fit)

    ind += iendmem_num
# -

array([ 1.46879207e-03, -1.61401333e+00,  1.00056641e-06,  1.12829730e-06])

# +

mask_new = ~np.isin(phase_names_soln, phase_stable)
inds = np.argsort(Aff_soln[mask_new])
print(Aff_soln[mask_new])
phase_names_soln[mask_new][Aff_soln[mask_new]<0]
# -




        comp_curr, dmu_curr =  get_current_phase_mu(Aff_soln, Aff_endmem, comp_soln, Ashft=Ashft)


        phs_endmem_sym =  get_phase_endmem_sym(phs_sym, endmem_ids)
        phase_name_curr = np.hstack((phase_names_soln, phs_endmem_sym))
        phase_name_parent = np.hstack((phase_names_soln, phs_sym))





# +

mu_stable_fin, dmudn_stable_fin, phase_endmem_num_stable_fin = build_stable_linear_phase_model(
    phase_stable, nmol_stable, T, P, phases)
# -

mu_stable_fin-extras['mu_fit']

mu_stable_fin-np.dot(chempot_stable,comp_endmem_stable.T)

# +
# mu0_stable+np.dot(dmudn_stable,nmol_stable - nmol0_stable) - mu_stable_fin
extras['mu0']+np.dot(extras['dmudn_stable'],nmol_stable - extras['nmol0_stable']) - mu_stable_fin


# -

mu0_stable = mu_stable_fin.copy()
dmudn_stable = dmudn_stable_fin.copy()
nmol0_stable = nmol_stable.copy()


chempot_stable

chempot_stable_fin = chempot_stable.copy()

chempot_stable-chempot_stable_fin





for iphsnm in phase_stable:
    iphs = phases[iphsnm]
    # print(iphs)
    iphs.chem



chempot_stable

nmol_stable

nmol_stable



# +

phase_stable, nmol_stable, comp_endmem_stable, extras = get_init_assem(bulk_comp, comp_endmem, mu_endmem,
                                                                       chempot_init=chempot_stable)
mu0_stable, dmudn_stable, phase_endmem_num_stable = build_stable_linear_phase_model(phase_stable, nmol_stable, T, P, phases)
np.dot(comp_endmem_stable.T, nmol_stable)-bulk_comp
# -

np.unique(phase_stable)

extras.keys()

extras['phase_n'] =

phase_names_soln_stable[extras['Aff_soln']<0]



comp_soln_stable, X_soln_stable, Aff_soln_stable, Aff_endmem_stable, phase_names_soln_stable, extras_stable = update_phase_affinities(chempot_stable)

Aff_soln_stable

phase_names_soln_stable[Aff_soln_stable<100]

phase_stable

dmudn_stable_next

dmudn_stable






        Aff_phases_fit = update_fitted_affinities(params, A, B, phase_endmem_num)
        dnmol = nmol_fit-nmol
        dchempot = chempot_fit-chempot

        resid = np.dot(A_lsq, params)-B_lsq
        chisqr = np.sqrt(np.mean(resid**2))





B_lsq



    B['Gtot'] = Gtot_init-Gshft

    Ascl_thresh = Ascl_max
    nmol = nmol_end0.copy()
    chempot = chempot_init.copy()


    Ascl_fac = np.exp(np.log(Ascl_max/Ascl_min)/Nsteps)


    count=1
    while True:

        # Affwt_endmem, Affwt = get_affinity_wt(Aff_phases, phase_endmem_num, Ascl_thresh)
        Affwt_endmem, Affwt = get_affinity_wt(Aff_phases_init, phase_endmem_num, Ascl_thresh)
        A_lsq, B_lsq = build_lstsq(A, B, Affwt_endmem, phase_endmem_num,
                                   sig_nmol_prior=sig_nmol_prior, sig_comp=sig_comp, sig_mu=sig_mu)

        output = np.linalg.lstsq(A_lsq, B_lsq, rcond=None)
        params = output[0]
        chempot_fit = params[:Nelems]
        nmol_fit = params[Nelems:]
        Aff_phases_fit = update_fitted_affinities(params, A, B, phase_endmem_num)
        dnmol = nmol_fit-nmol
        dchempot = chempot_fit-chempot

        resid = np.dot(A_lsq, params)-B_lsq
        chisqr = np.sqrt(np.mean(resid**2))


        if debug:
            print('chi2 = ', chisqr)
            print('Aff_phases_fit = ', Aff_phases_fit)
            print('sig. wt count = ', np.sum(Affwt_endmem>.05))
            print('Ascl_thresh = ', Ascl_thresh)
            print(np.max(dnmol))
            print('    dchempot = ',np.max(np.abs(dchempot)))
            print('----------')

        # if np.all(np.abs(dnmol) < TOL):
        #     break

        if count >= max_iter:
            break

        if np.all(np.abs(dchempot) < muTOL):
            break

        Aff_phases = Aff_phases_fit
        nmol = nmol_fit
        chempot = chempot_fit

        if Ascl_thresh > Ascl_min:
            Ascl_thresh /= Ascl_fac

        count += 1

    return chempot_fit, nmol_fit, Aff_phases_fit, Affwt







mu0_stable

# # Test

# +
# This takes ~1 sec due to inefficiency of affinity_and_comp
chempot_init, phase_stable, wt_assem, extras = get_init_assem(bulk_comp, comp_endmem, mu_endmem)

dmudn = extras['dmudn']
X = extras['X']
mu0 = extras['mu0']
comp = extras['elem_comp']
Aff_phases_init = extras['Aff_soln']


# +
extras['phase_name_parent']
# extras['phase_endmem_ids']

# phase_stable
extras.keys()

# +
Gtot_init = np.dot(chempot_init, bulk_comp)
print('Gtot = ', Gtot_init)

chempot = chempot_init.copy()
Aff_phases = Aff_phases_init.copy()

plt.figure()
plt.plot(Aff_phases/1e3, 'ko')
plt.ylim(-.5, .5)



# +


chempot_fit, nmol_fit, Aff_phases_fit, Affwt = discover_approx_hull(
    bulk_comp, chempot, Gtot_init, Aff_phases, mu0, dmudn, X,
    phase_endmem_num, comp_endmem,
    debug=True, sig_comp=1e-2, sig_nmol_prior=1.0,
    Ascl_min=1e-10, Ascl_max=1e-10, Nsteps=4, max_iter=8, Gshft=1e3)

Gtot = np.dot(chempot_fit, bulk_comp)
print('Gtot diff = ', Gtot-Gtot_init)
print('bulk_comp diff = ', np.dot(comp_endmem.T, nmol_fit )-bulk_comp)
# print('chempot_fit', chempot_fit)
# print('nmol_fit', nmol_fit)
# print('Aff_phases_fit', Aff_phases_fit)

comp_soln, X_soln, Aff_soln, Aff_endmem, phase_names_soln, extras = update_phase_affinities(chempot_fit)

dmudn = extras['dmudn']
X = extras['X']
mu0 = extras['mu0']

dchempot = chempot_fit-chempot
print(np.max(np.abs(dchempot)))

chempot = chempot_fit
Aff_phases = Aff_soln



# +

Affwt_endmem = np.repeat(Affwt, phase_endmem_num)
print(Affwt_endmem.size)



plt.figure()
plt.scatter(range(Aff_phases.size), Aff_phases/1e3, c=Affwt)
plt.plot(Aff_phases_init/1e3, 'rx')



plt.figure()
plt.scatter(range(nmol_fit.size), nmol_fit, c=Affwt_endmem)
# plt.scatter(Affwt, Aff_phases_init/1e3, 'rx')
# -

nmol_fit

# +
TOL = 0.1

ind = 0
bulk_approx = np.zeros(bulk_comp.size)
print(bulk_approx)

for iendmem,iwt, iphsnm in zip(phase_endmem_num,Affwt, phases):
    inmol = nmol_fit[ind:ind+iendmem]

    inmol[inmol<0] = 0


    icomp_end = comp_endmem[ind:ind+iendmem,:]
    iphs = phases[iphsnm]
    iX = X[iphsnm]

    if iwt > TOL:
        print('wt = ', iwt)
        print('nmol = ', inmol)
        print('iX = ', iX)
        bulk_approx += np.dot(icomp_end.T, inmol)

        if len(iX) > 1:
            ichempot = iphs.chem_potential(T, P, mol=inmol).squeeze()
            print('dchempot = ', ichempot-np.dot(icomp_end, chempot))
            print('ichempot = ', ichempot)



    print('---')


    ind += iendmem
# -

bulk_approx-bulk_comp





# +
# # %%timeit
# # %%snakeviz


# This takes ~1 sec due to inefficiency of affinity_and_comp
chempot_init, phase_stable, wt_assem, extras = get_init_assem(bulk_comp, comp_endmem, mu_endmem)

dmudn = extras['dmudn']
X = extras['X']
mu0 = extras['mu0']
comp = extras['elem_comp']
Aff_phases_init = extras['Aff_soln']
# -





# +
# np.dot(dmudn['Fsp'], np.ones(2))
# print(X.keys())
# [X[iphsnm].size for iphsnm in X]

# +
Nendmem = comp_endmem.shape[0]
phase_endmem_num = np.array([len(X[iphs]) for iphs in X])


Nphs = len(phase_endmem_num)
Npure = np.sum(phase_endmem_num==1)
Nsoln = np.sum(phase_endmem_num>1)

Nsoln_endmem = Nendmem-Npure
# -

chempot_fit, nmol_fit, Aff_phases_fit = discover_approx_hull(
    bulk_comp, chempot_init, Aff_phases_init, mu0, dmudn, X, phase_endmem_num, comp_endmem,
    debug=False, sig_nmol_prior=.1, Ascl_min=1, Ascl_max=1e3, Nsteps=10)

np.dot(nmol_fit, comp_endmem)-bulk_comp

plt.figure()
plt.plot(nmol_fit, 'ko')



(np.dot(comp_endmem,chempot)-np.dot(comp_endmem, chempot_init))/1e3

np.dot(comp_endmem, chempot_init)



# +

chempot = chempot_init.copy()
Aff_phases = Aff_phases_init.copy()

for ind in range(10):
    chempot_fit, nmol_fit, Aff_phases_fit = discover_approx_hull(
        bulk_comp, chempot, Aff_phases, mu0, dmudn, X, phase_endmem_num, comp_endmem,
        debug=False, sig_nmol_prior=1e10, Ascl_min=1, Ascl_max=1e3, Nsteps=10)

    print('chempot_fit', chempot_fit)
    print('nmol_fit', nmol_fit)
    print('Aff_phases_fit', Aff_phases_fit)

    comp_soln, X_soln, Aff_soln, Aff_endmem, phase_names_soln, extras = update_phase_affinities(chempot_fit)

    dmudn = extras['dmudn']
    X = extras['X']
    mu0 = extras['mu0']

    dchempot = chempot_fit-chempot
    print(np.max(np.abs(dchempot)))

    chempot = chempot_fit
    Aff_phases = Aff_soln


# -

chempot







Ascl_min = 1
Ascl = np.abs(np.median(Aff_soln[Aff_soln<=0]))
print(Ascl)
Ascl = np.max((Ascl, Ascl_min))


# +
Amag = Aff_soln/Ascl
Amask = Amag>0


wtstdt = (1+1/5*Amag[Amask]**2)**(-(5+1)/2)
wtcauchy = 1/(1+Amag[Amask]**2)


Awt = np.ones(Amag.size)
Awt[Amask] = wtstdt
# Awt[Amask] = wtcauchy

plt.figure()
# plt.semilogy(Awt, 'ko')
plt.plot(Awt, 'ko')

# plt.plot(Amag, Awt, 'ko')

# +

Awt_endmem = np.repeat(Awt, phase_endmem_num)
Awt_soln = Awt_endmem[:-Npure]
Awt_pure = Awt_endmem[-Npure:]
# -







# +

plt.figure()
plt.plot(Aff_soln/Ascl, 'ko')

# +

Agrid = np.linspace(-10, 10,1001)
wtgrid = np.ones(Agrid.size)

wtnorm = np.exp(-.5*Agrid**2)
wtcauchy = 1/(1+Agrid**2)
wtstdt = (1+1/5*Agrid**2)**(-(5+1)/2)

# wtgrid[Agrid>0] = wtnorm[Agrid>0]
# wtgrid[Agrid>0] = wtlog[Agrid>0]
# wtgrid[Agrid>0] = wtlaplace[Agrid>0]
# wtgrid[Agrid>0] = wtcauchy[Agrid>0]
wtgrid[Agrid>0] = wtstdt[Agrid>0]

# wtnorm
plt.figure()
plt.plot(Agrid, wtgrid, '-')
plt.xlim(-2, 10)
# -



# +
# X
# -





phsnm = 'Fsp'
phsnm = 'SplS'
phsnm = 'Cpx'
np.dot(dmudn[phsnm], X[phsnm])
idmudn = dmudn[phsnm]
iX = X[phsnm]
imu0 = mu0[phsnm]
icomp = comp[phsnm]
iphs_comp = phase_comps[phsnm]



idmudlogn = idmudn*iX
idlogndmu = np.linalg.pinv(idmudlogn)
np.dot(idmudlogn, np.ones(iX.size))

idndmu = np.linalg.pinv(idmudn)

np.dot(idmudn, idndmu)

M = np.dot(idmudlogn,idlogndmu)
U, S, Vh = np.linalg.svd(M)
print(S)

M = np.dot(idmudn,idndmu)
U, S, Vh = np.linalg.svd(M)
print(S)

np.dot(M, Vh[3])-Vh[3]

Vh[-1]/iX



# # Linear lst sqr refinement of current best phases

# +

mask_pure = np.tile(False, Nendmem)
mask_pure[-Npure:] = True
# -



# +
# phsnm = 'SplS'
# phsnm = 'Cpx'
# Nphs
# Npure
# Nsoln

Nparams = Nelems+Nendmem


M_tot = np.zeros((Nendmem, Nendmem))
dndmu_tot = np.zeros((Nendmem, Nendmem))
dmudn_tot = np.zeros((Nendmem, Nendmem))
nmol_end0 = np.zeros(Nendmem)
mu0_endmem = np.zeros(Nendmem)

ind_endmem = 0
for phsnm in mu0:
    idmudn = dmudn[phsnm]
    iX = X[phsnm]
    iendmem_num = iX.size

    imu0 = mu0[phsnm]
    icomp = comp[phsnm]
    iphs_comp = phase_comps[phsnm]

    idndmu = np.linalg.pinv(idmudn)
    iM = np.dot(idmudn, idndmu)

    M_tot[ind_endmem:ind_endmem+iendmem_num,
          ind_endmem:ind_endmem+iendmem_num] = iM

    dndmu_tot[ind_endmem:ind_endmem+iendmem_num,
              ind_endmem:ind_endmem+iendmem_num] = idndmu
    dmudn_tot[ind_endmem:ind_endmem+iendmem_num,
              ind_endmem:ind_endmem+iendmem_num] = idmudn

    nmol_end0[ind_endmem:ind_endmem+iendmem_num] = iX

    mu0_endmem[ind_endmem:ind_endmem+iendmem_num] = imu0

    # print(iM)
    # print(np.dot(iM, ))

    # iA_mu = np.dot(iM,iphs_comp)-iphs_comp
    # iB_mu = np.dot(iM, imu0) - imu0
    #
    #
    # idn0 = iX - np.dot(idndmu, imu0)
    # iB_comp = bulk_comp-np.dot(idn0, iphs_comp)
    #
    # idn_end = np.dot(idndmu, iphs_comp)
    # iA_comp = np.dot(iphs_comp.T, idn_end)

    # iA_comp = np.dot(iphs_comp, np.dot(idndmu, iphs_comp).T)
    # print(iphs_comp)
    # B_comp += iB_comp
    # A_comp += iA_comp

    # A_mu.append(iA_mu)
    # B_mu.append(iB_mu)

    ind_endmem += iendmem_num


# A_mu = np.vstack(A_mu)
# B_mu = np.hstack(B_mu)
# mu0_endmem = np.hstack(mu0_endmem)

# -



# +
# mask_2d_soln = np.dot(~mask_pure[:,np.newaxis],~mask_pure[np.newaxis,:])
# -

A_mu_pure = np.zeros((Npure, Nparams))
A_mu_pure[:,:Nelems] = comp_endmem[mask_pure,:]
B_mu_pure = mu0_endmem[mask_pure].copy()

# +
# NOTE: this relies on the pure elements being last!!!!

A_mu_soln = np.zeros((Nsoln_endmem, Nparams))
A_mu_soln[:,:Nelems] = comp_endmem[~mask_pure,:]
A_mu_soln[:,Nelems:Nelems+Nsoln_endmem] = -dmudn_tot[:-Npure,:-Npure]
B_mu_soln = mu0_endmem[:-Npure] - np.dot(dmudn_tot[:-Npure,:-Npure], nmol_end0[:-Npure])

# -

A_comp = np.zeros((Nelems, Nparams))
A_comp[:, -Nendmem:] = comp_endmem.T
B_comp = bulk_comp.copy()


# +
A_mu_comp = np.zeros((Nsoln_endmem, Nparams))
A_mu_comp[:,:Nelems] = -np.dot(dndmu_tot[:-Npure,:-Npure], comp_endmem[:-Npure, :])
A_mu_comp[:,Nelems:Nelems+Nsoln_endmem] = np.eye(Nsoln_endmem)
B_mu_comp = np.zeros(Nsoln_endmem)
B_mu_comp = nmol_end0[:-Npure] - np.dot(dndmu_tot[:-Npure,:-Npure], mu0_endmem[:-Npure])

# plt.figure()
# plt.imshow(A_mu_comp[:,:Nelems])
# plt.figure()
# plt.imshow(A_mu_comp[:,Nelems:Nelems+Nsoln_endmem])
# plt.figure()
# plt.imshow(A_mu_comp[:,-Npure:])

# -



# +
A_comp_prior = np.zeros((Nendmem, Nparams))
A_comp_prior[:,Nelems:] = np.eye(Nendmem)

B_comp_prior = np.zeros(Nendmem)

# -

A_mu_prior = np.zeros((1,Nparams))
A_mu_prior[0,0] = 1
B_mu_prior = np.zeros(1)
B_mu_prior

sys_elems

A_mu_pure.shape

A_mu_comp.shape

plt.figure()
plt.plot(Awt_pure, 'ko')
plt.plot(Awt_soln, 'rx')

# +
sig_comp = .01
sig_mu = 10
sig_comp_prior = sig_comp*3
sig_mu_prior = sig_mu*3

# single constraints on bulk_comp, mu_pure, mu_soln
A_lsq = np.vstack((A_mu_pure*Awt_pure[:,np.newaxis]/sig_mu,
                   A_mu_soln*Awt_soln[:,np.newaxis]/sig_mu,
                   A_comp/sig_comp,
                   A_comp_prior/sig_comp_prior,
                   A_mu_prior/sig_mu_prior))
B_lsq = np.hstack((B_mu_pure*Awt_pure/sig_mu,
                   B_mu_soln*Awt_soln/sig_mu,
                   B_comp/sig_comp,
                   B_comp_prior/sig_comp_prior,
                   B_mu_prior/sig_mu_prior))

# single constraints on bulk_comp, mu_pure, mu_soln
# A_lsq = np.vstack((A_mu_pure*Awt_pure[:,np.newaxis]/sig_mu,
#                    A_mu_soln*Awt_soln[:,np.newaxis]/sig_mu,
#                    A_comp/sig_comp))
# B_lsq = np.hstack((B_mu_pure*Awt_pure/sig_mu,
#                    B_mu_soln*Awt_soln/sig_mu,
#                    B_comp/sig_comp))

# Impose both fwd and bwd constraints on comp-mu link
# A_lsq = np.vstack((A_mu_pure*Awt_pure[:,np.newaxis]/sig_mu, A_mu_soln*Awt_soln[:,np.newaxis]/sig_mu,
#                    A_comp/sig_comp, A_mu_comp*Awt_soln[:,np.newaxis]/sig_comp))
# B_lsq = np.hstack((B_mu_pure*Awt_pure/sig_mu, B_mu_soln*Awt_soln/sig_mu,
#                    B_comp/sig_comp, B_mu_comp*Awt_soln/sig_comp))


# A_lsq = np.vstack((A_mu_pure*Awt_pure[:,np.newaxis]/sig_mu, A_mu_soln*Awt_soln[:,np.newaxis]/sig_mu,
#                    A_comp*Awt_pure[:,np.newaxis]/sig_comp, A_mu_comp*Awt_soln[:,np.newaxis]/sig_comp))
# B_lsq = np.hstack((B_mu_pure*Awt_pure/sig_mu, B_mu_soln*Awt_soln/sig_mu,
#                    B_comp*Awt_pure/sig_comp, B_mu_comp*Awt_soln/sig_comp))

# A_tot = np.vstack((A_mu/sig_mu, A_comp/sig_comp))
# B_tot = np.hstack((B_mu/sig_mu, B_comp/sig_comp))

output = np.linalg.lstsq(A_lsq, B_lsq, rcond=None)
params = output[0]
print('chempot_fit = ',params[:Nelems])
print('nmols = ', params[Nelems:])
# -

np.linalg.matrix_rank(A_lsq)

Awt_endmem.sum()

plt.figure()
plt.plot(params[Nelems:], 'ko')

plt.figure()
plt.plot(np.dot(A_lsq,params)-B_lsq, 'ko')





# +
nmols_fit = params[Nelems:]

dmu_pure_fit = B_mu_pure - np.dot(A_mu_pure, params)
dmu_soln_fit = B_mu_soln - np.dot(A_mu_soln, params)
dmu_fit = np.hstack((dmu_soln_fit, dmu_pure_fit))

ind_curr=0
Aff_soln_fit = []
for iendmem_num in phase_endmem_num:
    idmu = dmu_fit[ind_curr:ind_curr+iendmem_num]
    inmol = nmols_fit[ind_curr:ind_curr+iendmem_num]

    iAff = np.dot(inmol/np.sum(inmol),idmu)
    Aff_soln_fit.append(iAff)

    ind_curr += iendmem_num

Aff_soln_fit = np.array(Aff_soln_fit)
# -

Aff_soln_fit

Aff_soln

plt.figure()
plt.plot(Aff_soln/1e3, 'ko')
plt.plot(Aff_soln_fit/1e3, 'rx')
plt.ylim(-10,100)



Aff_soln = Aff_soln_fit













params[Nelems:]





params[Nelems:-Npure].shape

phase_endmem_num[:-Npure]

Aff_soln_fit





output

np.dot(comp_endmem.T, params[Nelems:])-bulk_comp

params[:Nelems]

params[Nelems:]

plt.figure()
plt.plot(params[Nelems:],'ko')
# plt.plot(params[Nelems:]*Awt_endmem,'ko')
# plt.plot(Awt_endmem,'rx')

sig_comp_prior

plt.figure()
plt.plot(params[Nelems:],'ko')
# plt.plot(params[Nelems:]*Awt_endmem,'ko')
# plt.plot(Awt_endmem,'rx')







# +
bnd_lo = np.tile(-np.inf, Nparams)
bnd_hi = np.tile(+np.inf, Nparams)
bnd_lo[-Npure:] = 0

output = opt.lsq_linear(A_lsq,B_lsq,bounds=(bnd_lo,bnd_hi))




# +
bnd_lo = np.tile(-np.inf, Nparams)
bnd_hi = np.tile(+np.inf, Nparams)
bnd_lo[-Npure:] = 0
# bnd_lo[:] = 0

output = opt.lsq_linear(A_lsq,B_lsq,bounds=(bnd_lo,bnd_hi))



# -

chempot_fit = output['x'][:Nelems]
nmol_fit = output['x'][Nelems:]


np.dot(comp_endmem.T, nmol_fit)-bulk_comp

# +
plt.figure()
plt.plot((chempot_fit-chempot)/1e3, 'ro')

plt.figure()
plt.plot(nmol_fit, 'ko')
plt.plot(params[Nelems:],'g^')
# plt.plot(Awt_endmem*nmol_fit, 'ko')
# plt.plot(Awt_endmem,'rx')
# plt.plot(params[Nelems:]*Awt_endmem,'g^')
# -







nmols = params[Nelems:]*Awt_endmem
plt.figure()
plt.plot(nmols, 'ko')
plt.plot(params[Nelems:], 'ro')
plt.plot(nmol_fit, 'co')

np.dot(comp_endmem.T, nmols)/bulk_comp

# +

np.dot(comp_endmem.T, params[Nelems:])-bulk_comp
np.dot(comp_endmem.T, nmol_fit)-bulk_comp
# -



# +
# phsnm = 'Fsp'
ind_endmem = 0
for phsnm in mu0:
    idmudn = dmudn[phsnm]
    iX = X[phsnm]
    iendmem_num = iX.size

    imu0 = mu0[phsnm]
    icomp = comp[phsnm]
    iphs_comp = phase_comps[phsnm]

    idndmu = np.linalg.pinv(idmudn)
    iM = np.dot(idmudn, idndmu)

    M_tot[ind_endmem:ind_endmem+iendmem_num,
          ind_endmem:ind_endmem+iendmem_num] = iM

    dndmu_tot[ind_endmem:ind_endmem+iendmem_num,
              ind_endmem:ind_endmem+iendmem_num] = idndmu
    dmudn_tot[ind_endmem:ind_endmem+iendmem_num,
              ind_endmem:ind_endmem+iendmem_num] = idmudn

    nmol_end0[ind_endmem:ind_endmem+iendmem_num] = iX
    # print(iM)
    # print(np.dot(iM, ))

    iA_mu = np.dot(iM,iphs_comp)-iphs_comp
    iB_mu = np.dot(iM, imu0) - imu0


    idn0 = iX - np.dot(idndmu, imu0)
    iB_comp = bulk_comp-np.dot(idn0, iphs_comp)

    idn_end = np.dot(idndmu, iphs_comp)
    iA_comp = np.dot(iphs_comp.T, idn_end)

    # iA_comp = np.dot(iphs_comp, np.dot(idndmu, iphs_comp).T)
    # print(iphs_comp)
    B_comp += iB_comp
    A_comp += iA_comp

    A_mu.append(iA_mu)
    B_mu.append(iB_mu)
    mu0_endmem.append(imu0)

    ind_endmem += iendmem_num



A_mu = np.vstack(A_mu)
B_mu = np.hstack(B_mu)
mu0_endmem = np.hstack(mu0_endmem)


# -



np.dot(comp_endmem, chempot)





# +
sig_comp = .001
sig_mu = 10

A_tot = np.vstack((A_mu/sig_mu, A_comp/sig_comp))
B_tot = np.hstack((B_mu/sig_mu, B_comp/sig_comp))

output = np.linalg.lstsq(A_tot, B_tot, rcond=None)
mu_elem = output[0]
mu_elem
# -

# Change in chempot
mu_change = np.dot(comp_endmem, mu_elem)-np.dot(comp_endmem, chempot)
plt.figure()
plt.plot(mu_change/1e3, 'ro')

(mu_elem-chempot)/1e3



# +
dmu_endmem = np.dot(comp_endmem, mu_elem)-mu0_endmem

plt.figure()
plt.plot((np.dot(M_tot, dmu_endmem)-dmu_endmem)/1e3, 'ko')

# +
nmol_end = nmol_end0 + np.dot(dndmu_tot, dmu_endmem)
plt.figure()
plt.plot(nmol_end, 'ko')
plt.plot(nmol_end[nmol_end<0], 'ro')


plt.figure()
plt.plot(nmol_end, dmu_endmem, 'ko')
# -





# +

np.dot(M_tot, mu0_endmem)
# -

np.dot(comp_endmem, mu_elem)

comp

nend_

# +
bulk_comp
# iphs_comp-
iphs_comp
idn0 = iX - np.dot(idndmu, imu0)
iB_comp = bulk_comp-np.dot(iphs_comp, idn0)

iB_mu = np.dot(iM, imu0) - imu0

# +
# hess = np.dot(A_mu.T,A_mu)
# np.linalg.svd(hess)

# +


np.linalg.lstsq(A_mu, B_mu, rcond=None)
# -

1e3*Vh[0]









display(Vh)
display(M)

# dmudn is rank deficient:
#  for a phase with D endmembers, there is only D-1 degrees of freedom to specify the chemical potential
# thus, must use pseudo inverse (which is least squares optimal)
idndmu = np.linalg.pinv(idmudn)



# +
idmu = np.zeros(imu0.size)
idmu = 1e3*np.random.randn(imu0.size)

np.dot(idndmu, idmu)
# -

np.linalg.det(idmudn)

# # %%timeit
U, S, Vh = np.linalg.svd(idmudn)
# Vh[-1]/iX

idndmu_p = np.linalg.pinv(idmudn)

display(idndmu_p)
display(idndmu)

np.linalg.pinv(idndmu_p)-idmudn

np.linalg.inv(idndmu) -idmudn

np.linalg.det(Vh[:-1])

Vh[-1]

print('phase_stable = ', phase_stable)
print('wt_assem = ', wt_assem)





# +
# X_soln
# -



# +
Nsoln = len(X_soln)

wt_assem_soln = wt_assem[:Nsoln]
wt_assem_endmem = wt_assem[Nsoln:]
# -



# +

mu_endmem_assem = []
comp_endmem_assem = []

wt_assem
comp_curr.shape
phase_name_curr
phase_assem = np.unique(phase_name_parent[wt_assem>0])
comp_tot = np.zeros(bulk_comp.size)
for iphsnm in phase_assem:
    iX = X_soln[iphsnm]
    imask_soln = phase_names==iphsnm
    imask_endmem = phs_sym==iphsnm

    iwt_soln = wt_assem_soln[imask_soln]
    iwt_endmem = wt_assem_endmem[imask_endmem]

    imol_tot = iwt_soln*iX + iwt_endmem
    # imol_tot = iX

    # icomp = np.dot(comp_endmem[imask_endmem,:].T, imol_tot)
    # comp_tot += icomp

    iphs = phases[iphsnm]

    iNelem = np.sum(iphs.props['element_comp'],axis=1)
    # print(iNelem)

    iNendmem = len(imol_tot)
    if iNendmem>1:
        imu = iphs.chem_potential(T, P, mol=imol_tot).squeeze()/iNelem
    else:
        imu = mu_endmem[imask_endmem].squeeze()

    icomp_endmem = comp_endmem[imask_endmem,:]
    # icomp_soln = np.dot(icomp_endmem,iX)
    # print(imu)
    # print(np.dot(icomp_soln.T, chempot))

    mu_endmem_assem.append(imu)
    comp_endmem_assem.append(icomp_endmem)

    # print('del(imu) = ', imu-mu_endmem[imask_endmem].squeeze())
    # print('soln: ')
    # print(iwt_soln)
    # print(iX)
    # print('endmem: ')
    # print(iwt_endmem)
    # print('imol_tot = ', imol_tot)
    # print('icomp = ', icomp)
    # print('----')

mu_endmem_assem = np.hstack(mu_endmem_assem)
comp_endmem_assem = np.vstack(comp_endmem_assem)
# -

comp_endmem_assem

output = np.linalg.lstsq(comp_endmem_assem, mu_endmem_assem, rcond=None)
chempot_update = output[0]
print(chempot_update-chempot)


# +

chempot=chempot_update
# -

chempot_update

chempot



# +

output = np.linalg.lstsq(comp_endmem, mu_endmem, rcond=None)
chempot_init = output[0]
chempot = chempot_init.copy()

# +
# linear only method with vertical offset, no update

Ashft = 1e3
comp_soln, Aff_soln, Aff_endmem, phase_names_soln, extras = update_phase_affinities(chempot)
comp_curr, dmu_curr =  get_current_phase_mu(Aff_soln, Aff_endmem, comp_soln, Ashft=Ashft)

dchempot_assem, Gtot, wt_assem, rnorm_assem = find_min_assemblage(
    bulk_comp, comp_curr, dmu_curr, chempot=0)


plt.figure()
plt.plot(wt_assem, 'ko-')

print('Gtot = ', Gtot)
chempot += dchempot_assem



inds = np.argsort(Aff_soln)
# print(phase_names_soln[inds][:rank])
# print(Aff_soln[inds][:rank])
# print(phase_names_soln[inds][rank:])
# print(Aff_soln[inds][rank:])

Aff_s = Aff_soln[inds]
print(phase_names_soln[inds][Aff_s<Ashft])
print(Aff_s[Aff_s<Ashft])

print('phs wts: ',phase_name_curr[wt_assem>0])

print('dchempot = ', dchempot_assem)


# -

np.dot(comp_curr.T,wt_assem)-bulk_comp








print(phase_names_soln)
print(phs_sym)
endmem_ids



# +
plt.figure()
plt.imshow(comp_soln[:,1:].T)
plt.ylabel('elem id')

plt.figure()
plt.plot(Aff_soln, 'ko')
# -



# +

comp_curr, dmu_curr, phase_names, soln_phase_names = get_current_phase_mu(
    phase_aff, comp_soln, comp_endmem=comp_endmem, phs_sym=phs_sym, Ashft=1e3)
# -





# +
# comp_curr

# +
# wt_bulk, rnorm_bulk = optimize.nnls(comp_soln_a.T,bulk_comp)
# if rnorm_bulk < 1e-8:
#     comp_curr = comp_soln_a
#     dmu_curr = Aff_soln_a
#     phase_name_curr = phase_names
# -

dchempot_assem


    inds = np.argsort(Aff_soln_a)
    print(phase_names[inds][:rank])
    print(Aff_soln_a[inds][:rank])
    print(phase_names[inds][rank:])
    print(Aff_soln_a[inds][rank:])

    Aff_s = Aff_soln_a[inds]
    print(Aff_s[Aff_s<100])
    print(phase_names[inds][Aff_s<100])

    dchempot_assem, Gtot, wt_assem, rnorm_assem = find_min_assemblage(
        bulk_comp, comp_curr, dmu_curr, chempot=0)
    print('Gtot = ', Gtot)

    print(phase_name_curr[wt_assem>0])
    print(np.unique(phase_parent_curr[wt_assem>0]))

    chempot_init += dchempot_assem





rank = np.linalg.matrix_rank(comp)
rank

# +
phs_endmem_sym = []
for iphs,id_num in zip(phs_sym,endmem_ids.astype(dtype=str)):
    iphsid = iphs+'_'+id_num
    phs_endmem_sym.append(iphsid)

phs_endmem_sym = np.array(phs_endmem_sym)



# +

output = np.linalg.lstsq(comp_endmem, mu_endmem, rcond=None)
chempot_init = output[0]
# -



# +


mu_soln0, dmudn_soln0, comp_soln0, X0, A0, natom,phase_names =  phase_affinity(
        chempot_init, phases, phase_comps,T, P)



# +
Aff_soln0_a = np.array(list(A0.values()))
comp_soln0_a = np.array(list(comp_soln0.values()))
comp_soln0_a.shape

comp_soln_a = comp_soln0_a.copy()
Aff_soln_a = Aff_soln0_a.copy()
# -



# +
mu_endmem_curr = np.dot(comp_endmem, chempot_init)
dmu_endmem = np.zeros(mu_endmem.size)

# Ashft = 1e3
# for iphs in A0:
#     iA = A0[iphs]
#     imask = phs_sym==iphs
#     dmu_endmem[imask] = iA+Ashft

Ashft = 1e3
for iphs in A0:
    iA = A0[iphs]
    iX = X0[iphs]
    imask = phs_sym==iphs
    dmu_endmem[imask] = iA+Ashft
# -

dmu_endmem

# +
dmu_curr = np.hstack((Aff_soln_a,dmu_endmem))
# dmu_curr

comp_curr = np.vstack((comp_soln_a, comp_endmem))
phase_name_curr = np.hstack((phase_names, phs_endmem_sym))
phase_parent_curr = np.hstack((phase_names, phs_sym))
# phase_name_curr
# comp_curr

# +
# wt_bulk, rnorm_bulk = optimize.nnls(comp_soln_a.T,bulk_comp)
# if rnorm_bulk < 1e-8:
#     comp_curr = comp_soln_a
#     dmu_curr = Aff_soln_a
#     phase_name_curr = phase_names

# +
inds = np.argsort(Aff_soln_a)
print(phase_names[inds][:rank])
print(Aff_soln_a[inds][:rank])
print(phase_names[inds][rank:])
print(Aff_soln_a[inds][rank:])

Aff_s = Aff_soln_a[inds]
print(Aff_s[Aff_s<100])
print(phase_names[inds][Aff_s<100])

# +

dchempot_assem, Gtot, wt_assem, rnorm_assem = find_min_assemblage(
    bulk_comp, comp_curr, dmu_curr, chempot=0)
print('Gtot = ', Gtot)
# -

print(phase_name_curr[wt_assem>0])
print(np.unique(phase_parent_curr[wt_assem>0]))

# +
# X0
# -

dchempot_assem

# +

chempot_init += dchempot_assem
# -
